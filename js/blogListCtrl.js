angular
    .module('blog')
    .controller('postListCtrl', ['$scope', '$location', '$localStorage', 'blogService', postListCtrl]);


function postListCtrl($scope, $location, $localStorage, blogService) {
	$scope.blogs = blogService.getAllPosts();
	/*
		Function for redirect to another page
	*/
	$scope.go = function (path) {
		$location.path(path);
	};
	/*
		Delete all posts function
	*/
	$scope.resetPosts = function(){
		$localStorage.$reset();
		$scope.blogs = blogService.getAllPosts();
	};
	/*
		Delete post
	*/
	$scope.deletePost = function(id){
		blogService.deletePost(id);
		$scope.blogs = blogService.getAllPosts();
	};
}



