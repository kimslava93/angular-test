angular
    .module('blog')
    .controller('postCreateCtrl', ['$scope', '$location', '$localStorage', 'blogService', postCreateCtrl]);


function postCreateCtrl($scope,  $location, $localStorage, blogService){ 	
	$scope.showDeleteButton = false;
	$scope.formTitle = "Create new post";
	$scope.postdata = {};
	$scope.postdata.date = new Date();
	
	/*
		Function for saving new post
	*/
	$scope.savePost = function(){
		$scope.parsedSaved = blogService.getAllPosts();
		if($scope.postdata.title == null || $scope.postdata.date == null) {
			alert('Please, fill all fields');
			return;
		}
		/*
			Push new object to the result array
		*/
		$scope.postdata.id = $scope.postdata.date.getTime();
		$scope.parsedSaved.push($scope.postdata);
		
		$localStorage.posts = JSON.stringify($scope.parsedSaved);
		$location.path('/');
	};
	/*
		Function for redirect to another page
	*/
	$scope.go = function (path) {
		$location.path(path);
	};

	$scope.deletePost = function(id){
		blogService.deletePost(id);
		$location.path('/');
	};
}







