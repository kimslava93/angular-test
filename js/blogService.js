angular.module('blog')
    .service('blogService', ['$localStorage', '$filter', '$location', blogService]);

    function blogService($localStorage, $filter, $location){
		/*
			Get post from all posts matched to the given id
			@param {int} id
			@return 
		*/
		this.getMatch = function(id){
			var parsedSaved = this.getAllPosts();
			var matches = $filter('filter')(parsedSaved, {id: id});
			return matches[0];																		
	    };
	    /*
			Get all posts from localStorage function
			@return {Object} post
	    */
	    this.getAllPosts = function(){
	    	var savedPosts = $localStorage.posts;
			return  (savedPosts !== undefined) ? JSON.parse(savedPosts) : []; 						
	    };
	    /*
			Returns index of element in array
			@param {Object} 
			@return {int} index of item in array
	    */
	    this.getIndex = function(itemId){
			var parsedSaved = this.getAllPosts();
			var matches = $filter('filter')(parsedSaved, {id: itemId})[0];
	    	return parsedSaved.indexOf(matches);
	    };
	    /*
			Delete post from localstorage
			@param {int} id of the post to delete
	    */
	    this.deletePost = function(id) {
	    	var savedPosts = this.getAllPosts();
	    	var index = this.getIndex(id);
			savedPosts.splice(index, 1);
			$localStorage.posts = JSON.stringify(savedPosts);
		};
	}