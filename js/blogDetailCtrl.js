angular
    .module('blog')
    .controller('postDetailCtrl', ['$scope', '$routeParams', '$filter', '$location', '$localStorage', 'blogService', postDetailCtrl]);


function postDetailCtrl($scope, $routeParams, $filter, $location, $localStorage, blogService){ 	
	$scope.showDeleteButton = true;
	$scope.formTitle = "Edit post";

	var posts = blogService.getAllPosts();
	var matches = blogService.getMatch($routeParams.id);
	var index = blogService.getIndex($routeParams.id);

	matches.date = new Date(matches.date);
	$scope.postdata = matches;
	/*
		Post delete function 
	*/
	$scope.deletePost = function(){
		posts.splice(index, 1);
		$localStorage.posts = JSON.stringify(posts);
		$location.path('/');
	};
	/*
		Function for redirect to another page
	*/
	$scope.go = function (path) {
		$location.path(path);
	};
	/*
		Update post function
	*/
	$scope.savePost = function(){
		posts[index].title = $scope.postdata.title;
		posts[index].author = $scope.postdata.author;
		posts[index].date = new Date($scope.postdata.date);
		posts[index].content = $scope.postdata.content;
		$localStorage.posts = JSON.stringify(posts);
		$location.path('/');
	};
}








