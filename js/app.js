var myapp = angular.module('blog', [
	'ngRoute',
	'ngStorage'
]);

myapp.config(['$routeProvider', function($routeProvider){
	$routeProvider
		.when('/blog', {
			templateUrl: '/views/blog.html',
			controller: 'postListCtrl'
		})
		.when('/blog/new', {
			templateUrl: '/views/post-create.html',
			controller: 'postCreateCtrl'
		})
		.when('/blog/:postId', {
			templateUrl: '/views/post-create.html',
			controller: 'postDetailCtrl'
		})
		.otherwise({
			redirectTo: '/blog'
		});
}]);
